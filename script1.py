import json
from openpyxl import load_workbook

def change_key(data, key, value, counter):
    """ Change dictionary key """
    if isinstance(data[key[counter]], dict):
        data = data[key[counter]]
        counter += 1
        change_key(data, key, value, counter)
    else:
        data[key[counter]] = value
    

with open('en.json', 'r') as file:
    data = json.load(file)

wb = load_workbook(filename = 'translation.xlsx')
sheet = wb['Sheet']

keys = [k[0].value for k in sheet['A1:A144']]
values = [v[0].value for v in sheet['B1:B144']]

final = []
for i in range(len(sheet['B1:B144'])):
    if values[i].find("\n") != -1:
        values[i] = values[i].split("\n")
    keys[i] = keys[i].split("-")
    final.append([keys[i], values[i]])

for i in final:
    change_key(data, i[0], i[1], 0)


with open('de.json', 'w', encoding='utf8') as file:
    json.dump(data, file, indent=2, ensure_ascii=False)


