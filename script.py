import json
from openpyxl import Workbook

with open('en.json', 'r') as file:
    data = json.load(file)

wb = Workbook()
sheet = wb.active

def rec(data, n):
    for i in data:
        a = []
        value = ""
        if n == "":
            name = str(i)
        else:
            name = n + "-" + str(i)
        t = type(data[i])
        if t == dict:
            sheet.append([name, ""])
            rec(data[i], name)
        elif t == list:
            value = ""
            for j in data[i]:
                if j == '':
                    value = "''\n"
                else:
                    value = value + str(j) + "\n"
            print(name, str(value))
            sheet.append([name, value])
        else:    
            value = data[i]
            print(name, str(value))
            sheet.append([name, value])
    wb.save('test.xlsx')

rec(data, "")
